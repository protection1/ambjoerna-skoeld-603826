<!DOCTYPE html>

<html xml:lang="en-US" lang="en-US"  >
	<head>


<script type="text/javascript" src="/static/js/analytics.js"></script>
<script type="text/javascript">archive_analytics.values.server_name="wwwb-app52.us.archive.org";archive_analytics.values.server_ms=0;</script>
<link type="text/css" rel="stylesheet" href="/static/css/banner-styles.css"/>


<meta charset="utf-8">

<title> Customer Support | CONTENT & SERVICES</title>
<meta name="keywords" content="Samsung">
<meta name="description" content="Samsung">
<meta name="copyright" content="Copyright &copy; Samsung Electronics Co., Ltd. All rights reserved.">
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />

<meta id="muse_viewPort" name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
	<link type="text/css" rel="stylesheet" href="/save/_embed/https://help.content.samsung.com/csweb/common/css/base_20190222.css" />
<link type="text/css" rel="stylesheet" href="/save/_embed/https://help.content.samsung.com/csweb/common/css/content_20190222.css" />
<link type="text/css" rel="stylesheet" href="/save/_embed/https://help.content.samsung.com/csweb/common/css/popup.css" />
<!--[if lte IE 8]><link type="text/css" rel="stylesheet" href="/csweb/common/css/ie8.css" /><![endif]-->
<!--[if lte IE 8]><script type="text/javascript" src="/csweb/common/js/html5.js"></script><![endif]--><!-- for IE6,7,8  -->
<!--[if IE]><script src="/csweb/common/js/respond.min.js"></script><![endif]-->

<script type="text/javascript" src="/save/_embed/https://help.content.samsung.com/csweb/common/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="/save/_embed/https://help.content.samsung.com/csweb/common/js/muse-1.0.2.js"></script>


<script type="text/javascript">

window.onresize = function(event) {
	Muse.init();
};

$(document).ready(function(){

	$('#mobile_odc_nav').on('click','.allMenu',function (e){
		$(this).toggleClass('active');
		$('#odcNav').toggle();
	});

	Muse.init();


});
$(window).load(function() {

 	var registReusltCookie = $.cookie("cs_registResult");

	 if(registReusltCookie != null ||registReusltCookie.length() > 0){
		 if("YP" == registReusltCookie ){
			 alert('Please go to \'My Questions\' to find your answer when you are notified via SMS.');
		 }else if("Y" == registReusltCookie ){
			 alert('Registration complete. The answer will be found in the Email you entered.');
		 }else if("ticketFail" == registReusltCookie ){
			 alert("Sorry.To execute the customer`s request, a problem has occurred.");
		 }else if("ticketFail2" == registReusltCookie ){
			 alert("Network Error!\n" + ' Sorry.To execute the customer`s request, a problem has occurred.') ;
		 }

		 registReusltCookie = "";
		 $.cookie("cs_registResult", registReusltCookie, {expire:0, path: '/', domain: '.samsung.com'});
	 }

});

</script>
</head>


<body>




  
    
<script> if (window.archive_analytics) { window.archive_analytics.values['server_name']="wwwb-app52.us.archive.org";}; </script>

<!-- Start of Record Banner output -->
<wb_div id="__wb_record_overlay_div" class="__wb_overlay">
</wb_div>

<wb_div class="__wb_record_content" id="__wb_record_content_loader">
<wb_h1 id="_wb_load_msg">Saving page now...</wb_h1>
<wb_p id="_wb_curr_url">https://help.content.samsung.com/csweb/main/main.do</wb_p>
<wb_p>As it appears live December 29, 2019 6:45:01 PM UTC</wb_p>



<wb_div id="__wb_spinningSquaresG">
<wb_div id="__wb_spinningSquaresG_1" class="__wb_spinningSquaresG">
</wb_div>
<wb_div id="__wb_spinningSquaresG_2" class="__wb_spinningSquaresG">
</wb_div>
<wb_div id="__wb_spinningSquaresG_3" class="__wb_spinningSquaresG">
</wb_div>
<wb_div id="__wb_spinningSquaresG_4" class="__wb_spinningSquaresG">
</wb_div>
<wb_div id="__wb_spinningSquaresG_5" class="__wb_spinningSquaresG">
</wb_div>
<wb_div id="__wb_spinningSquaresG_6" class="__wb_spinningSquaresG">
</wb_div>
<wb_div id="__wb_spinningSquaresG_7" class="__wb_spinningSquaresG">
</wb_div>
<wb_div id="__wb_spinningSquaresG_8" class="__wb_spinningSquaresG">
</wb_div>
</wb_div>

<img id="_wb_logo" src="/static/images/logo_WM.png"/>
</wb_div>

<script type="text/javascript" src="/static/js/disclaim-element.js" ></script>
<script type="text/javascript">
  function doRedir()
  {   
    var redirUrl = "/web/20191229184501/https://help.content.samsung.com/csweb/main/main.do";
    var textElem = document.getElementById("_wb_load_msg");
    if (textElem) {
      textElem.innerHTML = "Loading Saved Page...";
    }
    window.location.replace(redirUrl);  
  }
  
  function redirToCapture()
  {
    window.setTimeout(doRedir, 1000);
  }

  
  if ( window != window.top ) {
    
    var wmOverlay = document.getElementById("__wb_record_overlay");
    
    if (wmOverlay) {
      wmOverlay.style.display = "none";
    }
    
  } else {
    
    if (window.addEventListener) {
      window.addEventListener('load', redirToCapture, false);
    } else if (window.attachEvent) {
      window.attachEvent('onload', redirToCapture);
    }
    
    // Just in case, redir after 90 secs
    window.setTimeout(doRedir, 90000);
  }
</script>

<!-- End of Record Banner Output output -->

  




<input type="hidden" id="muse_deviceType" name="muse_deviceType" value="MOBILE"/>
<input type="hidden" id="muse_linkInChnl" name="muse_linkInChnl" value=""/>
<input type="hidden" id="muse_uri" name="muse_uri" value="/csweb/main/main.do"/>
<input type="hidden" id="muse_service" name="muse_service" value=""/>

<div id="muse_dimd" class="dimd" style="display:none;"></div>

<!--  Header -->
<header>
	<div id="headerIn">
		<h1 class="logo">
			<a class="sec bgIcon" href="/save/http://www.samsung.com/" target="_blank" title="새창"><em class="blind">Samsung</em></a>
		</h1>

		<div class="sign">
		<a href="#" id="muse_login" class="btnSign">Sign In</a>
			</div>
	</div>
</header>

<!-- Container -->
		<div id="museContainer">
			<div id="muse_common_nav"  >
				<div class="visual">
					<span>CONTENT &amp; SERVICES CUSTOMER SUPPORT</span>
					<p>HOW CAN WE HELP YOU?</p>
				</div>
				<nav>
					<ul id="museNav">
						<li class="mainLi"><a href="/save/https://help.content.samsung.com/csweb/main/main.do"><span>HOME</span> </a></li>
						<li class="faqLi cent"><a href="/save/https://help.content.samsung.com/csweb/faq/searchFaq.do" ><span>FAQ</span></a></li>
						<li class="ticketLi" data-svc-login="" data-login-yn="N"  data-phone-yn="Y"  >
							<a href="javascript:void(0)" class="last"><span>1:1 INQUIRY</span></a>
						</li>
					</ul>
				</nav>
			</div>

			<div id="mobile_odc_nav" class="odcTop" style="display:none;"  >
				<div class="odcHeader"   >
					<h1>Customer Support</h1>
					<p class=""><span class="appIcon"></span>
						Samsung</p>
				</div>

				<nav>
						<p>Menu</p>
						<!-- Samsung Health 이고 US 일 때 ODC 메뉴 버튼 안보이게 처리 -->
						<a href="javascript:void(0)" class="bgIcon allMenu"  >MENU</a><!-- 클릭스 active 클래스 추카-->
						<ul class="museLink" id="odcNav" style="display:none;">
							<li><a href="/save/https://help.content.samsung.com/csweb/faq/searchFaq.do">FAQ<span class="bgIcon"></span></a></li>
							<li class="inquiry" data-pass-yn="N"><a href="javascript:void(0)">Contact us<span class="bgIcon"></span></a></li>
							<li class="myList" data-pass-yn="N"><a href="javascript:void(0)">My questions<span class="bgIcon"></span></a></li>
							</ul>
				</nav>
			</div>

		<script type="text/javascript" src="/save/_embed/https://account.samsung.com/js/encTpLgnUtil.js" charset="UTF-8"></script>
<script type="text/javascript">
//<![CDATA[
var createTicketUrl = '/save/https://help.content.samsung.com/csweb'+ "/ticket/createQuestionTicket.do";
var searchTicketListUrl = '/save/https://help.content.samsung.com/csweb'+ "/ticket/searchTicketList.do";
var enquirygateUrl = '/save/https://help.content.samsung.com/csweb'+ "/main/ticketGate.do";

//add src  by cjs   : samsung members  chnl code = SMEM_ODC
// ""  = SMEM_ODC  재확인 필요 refUpTicket.positionTpCd ??
var chCd = "";
if(chCd == 'SMEM_ODC' ){
	 var searchTicketListUrl = '/save/https://help.content.samsung.com/csweb'+ "/refundacc/openSMRefundTicket.do?rfndParamKey="+"";
}

$(document).ready(function(){
	$.getScript('/save/https://account.samsung.com/account/encTpLgnUtil.do?serviceID=13w707y5dg&' + new Date().getTime());

	if(chCd == 'SMEM_ODC' ){ // 삼성 맴버스로 로그인시 해당 값이 없으므로 msap으로 넘겨받ID를 셋팅해줌
		document.museConfirmFrm.muse_con_svcIptLgnID.value = "";
	}

	(function(){
		$('.museLink').on('click', '.inquiry:not(.bgIcon)', function(e){
			checkTicksPass('N');
		}).on('click', '.myList:not(.bgIcon)', function(e){
			var passYn = $(this).attr("data-pass-yn") || '';

			

			if(passYn == 'Y' ){
				location.href=searchTicketListUrl;
			}else{
				confirmAccountLogin(searchTicketListUrl);
			}
		});

		$('#loginLink').on('click', function(e){
			confirmAccountLogin();
		});

		$('#museNav').on('click', '.ticketLi', function(e){
			// Samsung Health 이고 US 일 때, Contact Numbers 로 이동하고, 그 외에는 기존 로직 타도록 처리
			var svcCd =  $("input[name=svcCd]").val() == '' ? "" : $("input[name=svcCd]").val();
			var cntryCd = "US";

			if( svcCd == 'shealth' && cntryCd == 'US'){
				location.href= '/save/https://help.content.samsung.com/csweb'+ "/tutorial/searchContactNumbers.do";
			} else {
				var contactNumYn = $(this).attr("data-phone-yn") || '';
				checkTicksPass(contactNumYn);
			}
		});

		$('#headerIn').on('click', '#muse_login', function(e){
			confirmAccountLogin();
		}).on('click', '#muse_logout', function(e){
			$(location).attr('href',"/csweb/main/logout.ajax");
		});

		$('#muse_confirmLayer').on('keyup', '#muse_con_svcIptLgnPD', function(e){
			if( e.keyCode == 13 ) {
				confirmValidSubmit();
			}
		}).on('click', '#museConfrimBtn', function(e){
			confirmValidSubmit();
		}).on('focusout', '#muse_con_svcIptLgnPD', function(e){
			if($(this).val() != '') {
				$("#muse_con_svcIptLgnPDLabel").hide();
			}else{
				$("#muse_con_svcIptLgnPDLabel").show();
			}
		}).on('focusin', '#muse_con_svcIptLgnPD', function(e){
				$("#muse_con_svcIptLgnPDLabel").hide();
		});
	})();
});

function confirmValidSubmit(){
	var museFrm = document.museConfirmFrm;
	var iptLgnID = museFrm.muse_con_svcIptLgnID.value;
	var iptLgnPD = museFrm.muse_con_svcIptLgnPD.value;

	if( iptLgnPD < 1 ){
		alert("Please enter your password.");
		return false;
	}

	var cp = encTpLgnUtil(iptLgnID, iptLgnPD);

	var signInFrm = document.signInFrm;
	signInFrm.svcIptLgnID.value = cp.svcIptLgnID;
	signInFrm.svcIptLgnPD.value = cp.svcIptLgnPD;
	signInFrm.svcIptLgnKY.value = cp.svcIptLgnKY;
	signInFrm.svcIptLgnIV.value = cp.svcIptLgnIV;
	signInFrm.returnURL.value = searchTicketListUrl;
	signInFrm.submit();
}

function confirmAccountLogin(returnUrl){
	if(returnUrl){
		if( !confirm('Please login to use this service.')){
			return false;
		}
		if( returnUrl.length > 5) {
			document.signInFrm.returnURL.value = returnUrl;
		}
	}

	createStateValue();
}


function checkTicksPass(phoneFlag){
	$.ajax({
		type : "POST",
		url : "/csweb/main/checkTicketPass.ajax",
		data : { },
		dataType : "json"
	}).done(function(data, textStatus, jqXHR){
		var obj = data;

		if(obj.urlPass == 'Y') {
			location.href = createTicketUrl;
		}else if(phoneFlag == 'Y'){
			location.href = enquirygateUrl;
		}else {
			confirmAccountLogin(createTicketUrl);
		}
	});
}

function createStateValue(){
	$.ajax({
		type : "POST",
		url : "/csweb/main/createStateValue.ajax",
		data : { },
		dataType : "json"
	}).done(function(data, textStatus, jqXHR){
		var obj = data;
		var stateValue = obj.stateValue;

		document.signInFrm.state.value = stateValue;
		document.signInFrm.submit();
	});
}

//]]>
</script>
<div id="muse_confirmLayer" class="layerWrap" style="display:none">
	<h3>Confirm password</h3>
	<div class="layerContent">
		<p class="pwdIco"><span class="bgIcon">&nbsp;</span>Reenter your Samsung account password for verification.</p>
		<div class="loginBox pwd">
			<form name="museConfirmFrm" method="post" autocomplete="off" action="#muse_confirmLayer">
				<input type="hidden"  name="muse_con_svcIptLgnID" value="" />
				<input type="text" name="prevent" value="submit" style="display:none;"/>
				<div>
					<label for="muse_con_svcIptLgnPD" id="muse_con_svcIptLgnPDLabel">Password</label><input type="password" maxlength="50" title="Password" id="muse_con_svcIptLgnPD" name="muse_con_svcIptLgnPD"/>
				</div>
			</form>
			<button type="button" id="museConfrimBtn" class="museConfrimBtn" title="OK">OK</button>
			<form method="get" name="signInFrm" autocomplete="off" action="/save/https://account.samsung.com/accounts/v1/MUSE/signInGate" target="_self">
			<input type="hidden"   name="countryCode"        value="US" />
				<input type="hidden"   name="languageCode"       value="en" />
				<input type="hidden"   name="redirect_uri"       value="https://help.content.samsung.com/csweb/main/ssoLogin.do" />
				<input type="hidden"   name="goBackURL"          value="https://help.content.samsung.com/csweb/main/main.do" />
				<input type="hidden"   name="returnURL"          value="https://help.content.samsung.com/csweb/main/main.do" />
				<input type="hidden"   name="state"              value="" />
				<input type="hidden"   name="response_type"      value="code" />
				<input type="hidden"   name="client_id"          value="13w707y5dg"/>
			</form>
		</div>
	</div>
</div>

<!-- Contents -->
		<script type="text/javascript" src="/save/_embed/https://help.content.samsung.com/csweb/common/js/jquery.mobile.custom.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){

	document.title="HOME  | Customer Support |  CONTENT & SERVICES";


	(function(){
		$('#museNav').children('li').removeClass('on');
		$('.mainLi').addClass('on');
		changeVodArea(1);

		$('.visual').show();
		var svcCount = $('#mainSvcList li').size();
		var initHeight = $('#mainSvcList li:first-child .thumbBox').height() * 2 + 4;

		if(svcCount < 7 && Constants.MOBILE_SIZE < $(window).width() ){
			initHeight = initHeight / 2 ;
		}
		$('.serviceMask').css('height', initHeight);


		if($('#mainSvcList').height() < initHeight+30){
			$('.btnService.open').hide();
		}


		$('#museContent').on('click', '.serviceList > li .thumbBox ', function(e){
			var frm = document.forms['svc_menu'] ;
			frm.elements['svcCd'].value = $(this).attr('id');
			frm.action = "/csweb/faq/searchFaq.do";
			frm.submit();
		}).on('click', '.btnService.open > a', function(e){
			$('.serviceMask').css('height','');
			$(this).parent().hide();
			$('.btnService.close').show();

		}).on('click', '.btnService.close > a', function(e){
			$(this).parent().hide();
			$('.btnService.open').show();
			$('.serviceMask').css('height',initHeight);
		}).on('click', '.vodDimd, .playIco' ,function(e){
			var selectedVidoeEl = $(this).parent().parent('div.cont');

			var videoURL = selectedVidoeEl.attr('data-link');
			if($(window).width() < Constants.MOBILE_SIZE ){
				window.open(videoURL);
				return false;
			}

			var videoTitle = selectedVidoeEl.children('div').children('strong').html();
			var videoDesc = selectedVidoeEl.children('div').children('p').html();

			var vodLayer = $('.layerWrap.layerVod');
			vodLayer.children('.searchVod').children('h4').html(videoTitle);
			vodLayer.children('.searchVod').children('.txt').html(videoDesc);

			$('#muse_videoFrame').attr('src', videoURL);

			vodLayer.show();
			vodLayer.focus();
			$('.dimd').show();

		}).on('click','.btnPrev > a',function(e){

			var vodCount = $('.videoUl > li').size();
			var actvieVideoNum = $('.videoUl > li > a.num.active').attr('data-btn-num') || 0;
			actvieVideoNum = parseInt(actvieVideoNum) - 1;

			if(actvieVideoNum < 1 ){
				changeVodArea(vodCount);
			}else if(actvieVideoNum > vodCount){
				changeVodArea(1);
			}else{
				changeVodArea(actvieVideoNum);
			}
		}).on('click','.btnNext > a',function(e){

			var vodCount = $('.videoUl > li').size();
			var actvieVideoNum = $('.videoUl > li > a.num.active').attr('data-btn-num') || 0;
			actvieVideoNum = parseInt(actvieVideoNum) +1;

			if(actvieVideoNum < 1 ){
				changeVodArea(vodCount);
			}else if(actvieVideoNum > vodCount){
				changeVodArea(1);
			}else{
				changeVodArea(actvieVideoNum);
			}
		}).on('click','.videoRadio',function(e){
			var actvieVideoNum = $(this).attr('data-btn-num') || 0;
			actvieVideoNum = parseInt(actvieVideoNum);
			changeVodArea(actvieVideoNum);
		}).on('swiperight', '.vodList', function(e){
			e.preventDefault();
			if($(window).width() < Constants.MOBILE_SIZE ){
				$('.btnPrev > a').trigger('click');
			}
		}).on('swipeleft', '.vodList', function(e){
			e.preventDefault();
			if($(window).width() < Constants.MOBILE_SIZE ){
				$('.btnNext > a').trigger('click');
			}
		})
		;


	})();


});

function changeVodArea(cnt){
	var orderNumber = cnt || 1 ;
	var $videoEls =$('.videoUl').children('li');
	$videoEls.each(function(i, el){
		var $tEl = $(el);
		$tEl.children('.num').removeClass('active');
		$tEl.children('.cont').css('left','100%');
	});

	var selectedVideoEl =  $('.videoUl > li:nth-child(' + orderNumber +')');

	selectedVideoEl.children('.num').addClass('active');
	selectedVideoEl.children('.cont').css('left','');

}

function closeLayer(){
	$('.layerWrap.layerVod').hide();
	$('.dimd').hide();
	$('#muse_videoFrame').attr('src', '');
}


</script>
<div id="museContent">

	<h2>Choose a service</h2>

	<div class="serviceMask" style="min-heihgt:213px;">
		<ul id="mainSvcList"class="serviceList">

			<li class="">
				<div class="thumbBox" id="saccount">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						Samsung account</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="spay">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						Samsung Pay</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="apps">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						Galaxy Store</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="sconnect">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						SmartThings</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="samcloud">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						Samsung Cloud</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="shealth">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						Samsung Health</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="sgear">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						Galaxy Wearable (Samsung Gear)</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="sbrowser">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						Samsung Internet</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="themestore">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						Galaxy Themes</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="shlbixby">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						Bixby</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="glauncher">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						Game Launcher</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="findmymobile">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						Find My Mobile</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="bixbyv20">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						The New Bixby</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="bixbyhome">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						Bixby Home</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="sdex">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						Samsung DeX</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="smtswchmob">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						Samsung SmartSwitch Mobile</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="spass">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						Samsung Pass</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="spaygear">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						Samsung Pay (Watch)&lrm;</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="gbooster">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						Game Booster</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="swifi">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						Secure Wi-Fi</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="skidsmode">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						Kids Mode</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="sflow">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						Samsung Flow</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="penup">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						PENUP</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="skidshome">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						Samsung Kids Home</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="tizenstore">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						Tizen Store</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="ownershub">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						Samsung+</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="samsungvr">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						Samsung VR</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="smartswitch">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						Samsung Smart Switch</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="sbkeystore">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						Samsung Blockchain Keystore</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="sbwallet">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						Samsung Blockchain Wallet</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="seducate">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						S Educate</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="pictionary">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						Pictionary for Samsung TV</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="schefclct">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						Chef Collection</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			<li class="">
				<div class="thumbBox" id="printap">
					<a href="javascript:void(0)">
						<span class="icon"></span>
						<strong class="title">
						<span>
						Samsung PrinTap</span>
						</strong>
						<span class="bgIcon arrow"></span>
					</a>
				</div>

			</li>
			</ul>
	</div>


	<div class="btnService open bgNone">
		<a href="javascript:void(0)">VIEW MORE<span class="bgIcon"></span></a>
	</div>
	<div class="btnService close  bgNone" style="display:none"><a href="javascript:void(0)" class="close">CLOSE<span class="bgIcon"></span></a></div>
	<h2 class="helpTit bgNone" >Can&#39;t Find What You Need?</h2>

	<!-- VOD List -->

	<div class="quickLink museLink  " >
		<ul>
			<li>
				<a href="javascript:void(0)" class="inquiry" data-pass-yn="N" >
					<span class="bgIcon"></span>
					<strong>Contact us</strong>
					Ask your question in 1 on 1 Inquiry and we will give you a detailed answer.</a>
			</li>
			<li>
				<a href="javascript:void(0)" class="myList" data-pass-yn="N" >
					<span class="bgIcon"></span>
					<strong>My questions</strong>
					Check the answer of your questions on this page.</a>
			</li>
			<li>
				<a href="/save/https://help.content.samsung.com/csweb/tutorial/searchContactNumbers.do" class="tel">
					<span class="bgIcon"></span>
					<strong>Contact numbers</strong>
					Find the phone number for the Samsung Customer Service Center in your country.</a>
			</li>
			</ul>
	</div>

	<!-- //VOD List -->


<form method="post" name="svc_menu">
	<input type="hidden" name="searchKeyword"  value=""				/>
	<input type="hidden" name="svcCd"          value=""	/>
	<input type="hidden" name="langCd"		   value="en_us"	    />
	<input type="hidden" name="cntryCd"		   value="US"	/>
</form>


</div>


<div id="muse_vodLayer"class="layerWrap layerVod" style="display:none" tabindex="0">
		<h3>Video Guide</h3>

		<div class="searchVod">
			<h4></h4>
			<div class="player">
				<iframe id="muse_videoFrame" width="100%" height="100%" frameborder="0" allowfullscreen></iframe>
			</div>
			<div class="txt">
			</div>

		</div>
		<a href="javascript:void(0)" onClick="closeLayer();return false;" class="bgIcon layerCloseBtn ">Close</a>
</div>

</div>
	<script type="text/javascript">
$(document).ready(function(){

	$('#muse_footer').on('click', '#muse_web_cntry', function(e){

    	var svcCd = $('#muse_service').val() || '';

    	if (svcCd.length > 2 ){
			 $.ajax({
					type:'POST',
					url:'/csweb/main/goUrlByLang.ajax',
					data  : 'ticketId=' + svcCd,
					dataType: 'json' ,
					success:function(data){
						var obj = data ;
						if(obj.result == 'Y') {
							document.muse_langFrm.action = '/csweb/main/selectLocationList.do';
							document.muse_langFrm.submit();
						}
					}
			});

		}else{
			$(location).attr('href','/csweb/main/selectLocationList.do');
		}
	}).on('change', '#language_select', function(e){
		var cntryLang = jQuery.parseJSON($(this).val()) || '';
		if(cntryLang.cntryCd.length > 1){
			$("#language_select option:selected").removeAttr('selected');
			$("#language_select option:eq(0)").attr("selected", "selected");
			document.muse_langFrm.cntry_cd.value = cntryLang.cntryCd;
			document.muse_langFrm.lang_cd.value = cntryLang.langCd;
			document.muse_langFrm.submit();
		}

	});



});


function logOut(){
	location.href = "/csweb/main/logout.ajax";
}
</script>


<footer id="muse_footer">
	<form name="muse_langFrm" method="post" action="/save/https://help.content.samsung.com/csweb/main/changeLocation.do">
		<input type="hidden" name="cntry_cd" value=""/>
		<input type="hidden" name="lang_cd" value=""/>
		<input type="hidden" name="returnUrl" value="/csweb/main/main.do"/>
	</form>


	<div class="footerIn">
		<div class="mem_box">
<div class="login"><a id="loginLink" href="javascript:void(0)">Sign In</a></div>
<div class="pcLang" >
				<div class="current">
					<a id="muse_web_cntry" href="javascript:void(0)"  style="cursor: pointer;">
						USA / English (US)<span class="bgIcon">&nbsp;</span>
					</a>
				</div>
			</div>

			<select class="lang" title="Select Language" id="language_select" style="border: 1px solid rgb(202, 203, 207);">
				<option value="" selected>USA - English (US)</option>
				<option value=' {"cntryCd":"AF", "langCd":"en_gb"} '  >
						Afghanistan - English</option>
					<option value=' {"cntryCd":"AL", "langCd":"en_gb"} '  >
						Albania - English</option>
					<option value=' {"cntryCd":"DZ", "langCd":"en_us"} '  >
						Algeria - English</option>
					<option value=' {"cntryCd":"DZ", "langCd":"ar_ae"} '  >
						Algeria - العربية</option>
					<option value=' {"cntryCd":"AO", "langCd":"fr_fr"} '  >
						Angola - Francais</option>
					<option value=' {"cntryCd":"AO", "langCd":"en_gb"} '  >
						Angola - English</option>
					<option value=' {"cntryCd":"AI", "langCd":"en_us"} '  >
						Anguilla - English</option>
					<option value=' {"cntryCd":"AI", "langCd":"es_latn"} '  >
						Anguilla - Espanol</option>
					<option value=' {"cntryCd":"AG", "langCd":"en_us"} '  >
						Antigua and Barbuda - English</option>
					<option value=' {"cntryCd":"AG", "langCd":"es_latn"} '  >
						Antigua and Barbuda - Espanol</option>
					<option value=' {"cntryCd":"AR", "langCd":"es_latn"} '  >
						Argentina - Espanol</option>
					<option value=' {"cntryCd":"AM", "langCd":"en_gb"} '  >
						Armenia - English</option>
					<option value=' {"cntryCd":"AW", "langCd":"en_us"} '  >
						Aruba - English</option>
					<option value=' {"cntryCd":"AW", "langCd":"es_latn"} '  >
						Aruba - Espanol</option>
					<option value=' {"cntryCd":"AU", "langCd":"en_gb"} '  >
						Australia - English</option>
					<option value=' {"cntryCd":"AT", "langCd":"de"} '  >
						Austria - Deutsch</option>
					<option value=' {"cntryCd":"AZ", "langCd":"en_gb"} '  >
						Azerbaijan - English</option>
					<option value=' {"cntryCd":"BS", "langCd":"en_us"} '  >
						Bahamas - English</option>
					<option value=' {"cntryCd":"BS", "langCd":"es_latn"} '  >
						Bahamas - Espanol</option>
					<option value=' {"cntryCd":"BH", "langCd":"en_us"} '  >
						Bahrain - English</option>
					<option value=' {"cntryCd":"BH", "langCd":"ar_ae"} '  >
						Bahrain - العربية</option>
					<option value=' {"cntryCd":"BD", "langCd":"en_gb"} '  >
						Bangladesh - English</option>
					<option value=' {"cntryCd":"BB", "langCd":"en_us"} '  >
						Barbados - English</option>
					<option value=' {"cntryCd":"BB", "langCd":"es_latn"} '  >
						Barbados - Espanol</option>
					<option value=' {"cntryCd":"BY", "langCd":"ru"} '  >
						Belarus - Русский</option>
					<option value=' {"cntryCd":"BE", "langCd":"fr_fr"} '  >
						Belgium - Francais</option>
					<option value=' {"cntryCd":"BE", "langCd":"nl"} '  >
						Belgium - Nederlands</option>
					<option value=' {"cntryCd":"BZ", "langCd":"en_us"} '  >
						Belize - English</option>
					<option value=' {"cntryCd":"BZ", "langCd":"es_latn"} '  >
						Belize - Espanol</option>
					<option value=' {"cntryCd":"BJ", "langCd":"en_gb"} '  >
						Benin - English</option>
					<option value=' {"cntryCd":"BJ", "langCd":"fr_fr"} '  >
						Benin - Francais</option>
					<option value=' {"cntryCd":"BM", "langCd":"en_us"} '  >
						Bermuda - English</option>
					<option value=' {"cntryCd":"BM", "langCd":"es_latn"} '  >
						Bermuda - Espanol</option>
					<option value=' {"cntryCd":"BO", "langCd":"en_gb"} '  >
						Bolivia - English</option>
					<option value=' {"cntryCd":"BO", "langCd":"es_latn"} '  >
						Bolivia - Espanol</option>
					<option value=' {"cntryCd":"BA", "langCd":"en_gb"} '  >
						Bosnia and Herzegovina - English</option>
					<option value=' {"cntryCd":"BW", "langCd":"fr_fr"} '  >
						Botswana - Francais</option>
					<option value=' {"cntryCd":"BW", "langCd":"en_gb"} '  >
						Botswana - English</option>
					<option value=' {"cntryCd":"BR", "langCd":"pt_latn"} '  >
						Brazil - Português</option>
					<option value=' {"cntryCd":"VG", "langCd":"en_us"} '  >
						British Virgin Islands - English</option>
					<option value=' {"cntryCd":"VG", "langCd":"es_latn"} '  >
						British Virgin Islands - Espanol</option>
					<option value=' {"cntryCd":"BN", "langCd":"en_us"} '  >
						Brunei - English</option>
					<option value=' {"cntryCd":"BG", "langCd":"bg"} '  >
						Bulgaria - Български</option>
					<option value=' {"cntryCd":"BF", "langCd":"fr_fr"} '  >
						Burkina Faso - Francais</option>
					<option value=' {"cntryCd":"BF", "langCd":"en_us"} '  >
						Burkina Faso - English</option>
					<option value=' {"cntryCd":"BI", "langCd":"en_gb"} '  >
						Burundi - English</option>
					<option value=' {"cntryCd":"BI", "langCd":"fr_fr"} '  >
						Burundi - Francais</option>
					<option value=' {"cntryCd":"KH", "langCd":"en_gb"} '  >
						Cambodia - English</option>
					<option value=' {"cntryCd":"CM", "langCd":"fr_fr"} '  >
						Cameroon - Francais</option>
					<option value=' {"cntryCd":"CM", "langCd":"en_gb"} '  >
						Cameroon - English</option>
					<option value=' {"cntryCd":"CA", "langCd":"fr_ca"} '  >
						Canada - Francais</option>
					<option value=' {"cntryCd":"CA", "langCd":"en_us"} '  >
						Canada - English</option>
					<option value=' {"cntryCd":"KY", "langCd":"en_us"} '  >
						Cayman Islands - English</option>
					<option value=' {"cntryCd":"KY", "langCd":"es_latn"} '  >
						Cayman Islands - Espanol</option>
					<option value=' {"cntryCd":"CF", "langCd":"en_gb"} '  >
						Central African Republic - English</option>
					<option value=' {"cntryCd":"CF", "langCd":"fr_fr"} '  >
						Central African Republic - Francais</option>
					<option value=' {"cntryCd":"CL", "langCd":"es_latn"} '  >
						Chile - Espanol</option>
					<option value=' {"cntryCd":"CN", "langCd":"zh_cn"} '  >
						China - 中文&rlm;(简体)</option>
					<option value=' {"cntryCd":"CO", "langCd":"es_latn"} '  >
						Colombia - Espanol</option>
					<option value=' {"cntryCd":"CG", "langCd":"fr_fr"} '  >
						Congo - Francais</option>
					<option value=' {"cntryCd":"CG", "langCd":"en_gb"} '  >
						Congo - English</option>
					<option value=' {"cntryCd":"CK", "langCd":"en_us"} '  >
						Cook Islands - English</option>
					<option value=' {"cntryCd":"CR", "langCd":"es_latn"} '  >
						Costa Rica - Espanol</option>
					<option value=' {"cntryCd":"CI", "langCd":"fr_fr"} '  >
						Cote D'Ivoire - Francais</option>
					<option value=' {"cntryCd":"CI", "langCd":"en_gb"} '  >
						Cote D'Ivoire - English</option>
					<option value=' {"cntryCd":"HR", "langCd":"hr"} '  >
						Croatia - Hrvatski</option>
					<option value=' {"cntryCd":"CU", "langCd":"es_latn"} '  >
						Cuba - Espanol</option>
					<option value=' {"cntryCd":"CW", "langCd":"en_us"} '  >
						Curacao - English</option>
					<option value=' {"cntryCd":"CW", "langCd":"es_latn"} '  >
						Curacao - Espanol</option>
					<option value=' {"cntryCd":"CY", "langCd":"en_gb"} '  >
						Cyprus - English</option>
					<option value=' {"cntryCd":"CZ", "langCd":"cs"} '  >
						Czech Republic - Čeština</option>
					<option value=' {"cntryCd":"DK", "langCd":"da"} '  >
						Denmark - Dansk</option>
					<option value=' {"cntryCd":"DM", "langCd":"en_us"} '  >
						Dominica - English</option>
					<option value=' {"cntryCd":"DM", "langCd":"es_latn"} '  >
						Dominica - Espanol</option>
					<option value=' {"cntryCd":"DO", "langCd":"es_latn"} '  >
						Dominican Republic - Espanol</option>
					<option value=' {"cntryCd":"CD", "langCd":"fr_fr"} '  >
						DR Congo - Francais</option>
					<option value=' {"cntryCd":"CD", "langCd":"en_gb"} '  >
						DR Congo - English</option>
					<option value=' {"cntryCd":"EC", "langCd":"es_latn"} '  >
						Ecuador - Espanol</option>
					<option value=' {"cntryCd":"EG", "langCd":"ar_ae"} '  >
						Egypt - العربية</option>
					<option value=' {"cntryCd":"SV", "langCd":"es_latn"} '  >
						El Salvador - Espanol</option>
					<option value=' {"cntryCd":"EE", "langCd":"et"} '  >
						Estonia - Eesti</option>
					<option value=' {"cntryCd":"ET", "langCd":"fr_fr"} '  >
						Ethiopia - Francais</option>
					<option value=' {"cntryCd":"ET", "langCd":"en_gb"} '  >
						Ethiopia - English</option>
					<option value=' {"cntryCd":"FJ", "langCd":"en_us"} '  >
						Fiji - English</option>
					<option value=' {"cntryCd":"FI", "langCd":"fi"} '  >
						Finland - Suomi</option>
					<option value=' {"cntryCd":"FR", "langCd":"fr_fr"} '  >
						France - Francais</option>
					<option value=' {"cntryCd":"MK", "langCd":"mk"} '  >
						FYROM - Македонски</option>
					<option value=' {"cntryCd":"GA", "langCd":"en_gb"} '  >
						Gabon - English</option>
					<option value=' {"cntryCd":"GA", "langCd":"fr_fr"} '  >
						Gabon - Francais</option>
					<option value=' {"cntryCd":"GM", "langCd":"fr_fr"} '  >
						Gambia - Francais</option>
					<option value=' {"cntryCd":"GM", "langCd":"en_us"} '  >
						Gambia - English</option>
					<option value=' {"cntryCd":"GE", "langCd":"en_gb"} '  >
						Georgia - English</option>
					<option value=' {"cntryCd":"DE", "langCd":"de"} '  >
						Germany - Deutsch</option>
					<option value=' {"cntryCd":"GH", "langCd":"fr_fr"} '  >
						Ghana - Francais</option>
					<option value=' {"cntryCd":"GH", "langCd":"en_gb"} '  >
						Ghana - English</option>
					<option value=' {"cntryCd":"GR", "langCd":"el"} '  >
						Greece - Ελληνικά</option>
					<option value=' {"cntryCd":"GL", "langCd":"en_us"} '  >
						Greenland - English</option>
					<option value=' {"cntryCd":"GD", "langCd":"en_us"} '  >
						Grenada - English</option>
					<option value=' {"cntryCd":"GD", "langCd":"es_latn"} '  >
						Grenada - Espanol</option>
					<option value=' {"cntryCd":"GT", "langCd":"es_latn"} '  >
						Guatemala - Espanol</option>
					<option value=' {"cntryCd":"GN", "langCd":"en_gb"} '  >
						Guinea - English</option>
					<option value=' {"cntryCd":"GN", "langCd":"fr_fr"} '  >
						Guinea - Francais</option>
					<option value=' {"cntryCd":"GY", "langCd":"en_us"} '  >
						Guyana - English</option>
					<option value=' {"cntryCd":"GY", "langCd":"es_latn"} '  >
						Guyana - Espanol</option>
					<option value=' {"cntryCd":"HT", "langCd":"en_us"} '  >
						Haiti - English</option>
					<option value=' {"cntryCd":"HT", "langCd":"es_latn"} '  >
						Haiti - Espanol</option>
					<option value=' {"cntryCd":"HN", "langCd":"es_latn"} '  >
						Honduras - Espanol</option>
					<option value=' {"cntryCd":"HK", "langCd":"en_gb"} '  >
						Hong Kong - English</option>
					<option value=' {"cntryCd":"HK", "langCd":"zh_hk"} '  >
						Hong Kong - 中文&rlm;(香港)</option>
					<option value=' {"cntryCd":"HU", "langCd":"hu"} '  >
						Hungary - Magyar</option>
					<option value=' {"cntryCd":"IS", "langCd":"en_gb"} '  >
						Iceland - English</option>
					<option value=' {"cntryCd":"IN", "langCd":"en_gb"} '  >
						India - English</option>
					<option value=' {"cntryCd":"ID", "langCd":"id"} '  >
						Indonesia - Bahasa Indonesia</option>
					<option value=' {"cntryCd":"IR", "langCd":"fa"} '  >
						Iran, Islamic Republic Of - فارسی</option>
					<option value=' {"cntryCd":"IQ", "langCd":"en_us"} '  >
						Iraq - English</option>
					<option value=' {"cntryCd":"IQ", "langCd":"ar_ae"} '  >
						Iraq - العربية</option>
					<option value=' {"cntryCd":"IE", "langCd":"gd"} '  >
						Ireland - Gaelic</option>
					<option value=' {"cntryCd":"IE", "langCd":"en_gb"} '  >
						Ireland - English</option>
					<option value=' {"cntryCd":"IL", "langCd":"iw"} '  >
						Israel - עברית</option>
					<option value=' {"cntryCd":"IT", "langCd":"it"} '  >
						Italy - Italiano</option>
					<option value=' {"cntryCd":"JM", "langCd":"en_gb"} '  >
						Jamaica - English</option>
					<option value=' {"cntryCd":"JP", "langCd":"ja"} '  >
						Japan - 日本語</option>
					<option value=' {"cntryCd":"JO", "langCd":"ar_ae"} '  >
						Jordan - العربية</option>
					<option value=' {"cntryCd":"KZ", "langCd":"kk"} '  >
						Kazakhstan - Қазақша</option>
					<option value=' {"cntryCd":"KZ", "langCd":"ru"} '  >
						Kazakhstan - Русский</option>
					<option value=' {"cntryCd":"KE", "langCd":"en_gb"} '  >
						Kenya - English</option>
					<option value=' {"cntryCd":"KR", "langCd":"ko"} '  >
						Korea, South - 한국어</option>
					<option value=' {"cntryCd":"KW", "langCd":"ar_ae"} '  >
						Kuwait - العربية</option>
					<option value=' {"cntryCd":"KG", "langCd":"en_gb"} '  >
						Kyrgyzstan - English</option>
					<option value=' {"cntryCd":"LA", "langCd":"en_gb"} '  >
						Laos - English</option>
					<option value=' {"cntryCd":"LV", "langCd":"lv"} '  >
						Latvia - Latviešu valodā</option>
					<option value=' {"cntryCd":"LB", "langCd":"en_us"} '  >
						Lebanon - English</option>
					<option value=' {"cntryCd":"LB", "langCd":"ar_ae"} '  >
						Lebanon - العربية</option>
					<option value=' {"cntryCd":"LS", "langCd":"fr_fr"} '  >
						Lesotho - Francais</option>
					<option value=' {"cntryCd":"LS", "langCd":"en_gb"} '  >
						Lesotho - English</option>
					<option value=' {"cntryCd":"LR", "langCd":"fr_fr"} '  >
						Liberia - Francais</option>
					<option value=' {"cntryCd":"LR", "langCd":"en_gb"} '  >
						Liberia - English</option>
					<option value=' {"cntryCd":"LY", "langCd":"en_us"} '  >
						Libya - English</option>
					<option value=' {"cntryCd":"LY", "langCd":"ar_ae"} '  >
						Libya - العربية</option>
					<option value=' {"cntryCd":"LI", "langCd":"en_us"} '  >
						Liechtenstein - English</option>
					<option value=' {"cntryCd":"LT", "langCd":"lt"} '  >
						Lithuania - Lietuvių kalba</option>
					<option value=' {"cntryCd":"LU", "langCd":"fr_fr"} '  >
						Luxembourg - Francais</option>
					<option value=' {"cntryCd":"MO", "langCd":"en_gb"} '  >
						Macao - English</option>
					<option value=' {"cntryCd":"MG", "langCd":"en_gb"} '  >
						Madagascar - English</option>
					<option value=' {"cntryCd":"MG", "langCd":"fr_fr"} '  >
						Madagascar - Francais</option>
					<option value=' {"cntryCd":"MW", "langCd":"fr_fr"} '  >
						Malawi - Francais</option>
					<option value=' {"cntryCd":"MW", "langCd":"en_us"} '  >
						Malawi - English</option>
					<option value=' {"cntryCd":"MY", "langCd":"ms"} '  >
						Malaysia - Bahasa Melayu</option>
					<option value=' {"cntryCd":"MY", "langCd":"en_us"} '  >
						Malaysia - English</option>
					<option value=' {"cntryCd":"MV", "langCd":"en_us"} '  >
						Maldives - English</option>
					<option value=' {"cntryCd":"ML", "langCd":"fr_fr"} '  >
						Mali - Francais</option>
					<option value=' {"cntryCd":"ML", "langCd":"en_us"} '  >
						Mali - English</option>
					<option value=' {"cntryCd":"MT", "langCd":"en_gb"} '  >
						Malta - English</option>
					<option value=' {"cntryCd":"MR", "langCd":"fr_fr"} '  >
						Mauritania - Francais</option>
					<option value=' {"cntryCd":"MR", "langCd":"en_us"} '  >
						Mauritania - English</option>
					<option value=' {"cntryCd":"MU", "langCd":"fr_fr"} '  >
						Mauritius - Francais</option>
					<option value=' {"cntryCd":"MU", "langCd":"en_gb"} '  >
						Mauritius - English</option>
					<option value=' {"cntryCd":"MX", "langCd":"es_latn"} '  >
						Mexico - Espanol</option>
					<option value=' {"cntryCd":"MD", "langCd":"ro"} '  >
						Moldova - Română</option>
					<option value=' {"cntryCd":"MD", "langCd":"en_gb"} '  >
						Moldova - English</option>
					<option value=' {"cntryCd":"MC", "langCd":"en_us"} '  >
						Monaco - English</option>
					<option value=' {"cntryCd":"MN", "langCd":"en_gb"} '  >
						Mongolia - English</option>
					<option value=' {"cntryCd":"ME", "langCd":"en_gb"} '  >
						Montenegro - English</option>
					<option value=' {"cntryCd":"MS", "langCd":"en_us"} '  >
						Montserrat - English</option>
					<option value=' {"cntryCd":"MS", "langCd":"es_latn"} '  >
						Montserrat - Espanol</option>
					<option value=' {"cntryCd":"MA", "langCd":"fr_fr"} '  >
						Morocco - Francais</option>
					<option value=' {"cntryCd":"MZ", "langCd":"fr_fr"} '  >
						Mozambique - Francais</option>
					<option value=' {"cntryCd":"MZ", "langCd":"en_gb"} '  >
						Mozambique - English</option>
					<option value=' {"cntryCd":"MM", "langCd":"en_gb"} '  >
						Myanmar - English</option>
					<option value=' {"cntryCd":"NA", "langCd":"fr_fr"} '  >
						Namibia - Francais</option>
					<option value=' {"cntryCd":"NA", "langCd":"en_gb"} '  >
						Namibia - English</option>
					<option value=' {"cntryCd":"NP", "langCd":"en_gb"} '  >
						Nepal - English</option>
					<option value=' {"cntryCd":"NL", "langCd":"nl"} '  >
						Netherlands - Nederlands</option>
					<option value=' {"cntryCd":"NZ", "langCd":"en_gb"} '  >
						New Zealand - English</option>
					<option value=' {"cntryCd":"NI", "langCd":"es_latn"} '  >
						Nicaragua - Espanol</option>
					<option value=' {"cntryCd":"NG", "langCd":"fr_fr"} '  >
						Nigeria - Francais</option>
					<option value=' {"cntryCd":"NG", "langCd":"en_gb"} '  >
						Nigeria - English</option>
					<option value=' {"cntryCd":"NO", "langCd":"no"} '  >
						Norway - Norsk</option>
					<option value=' {"cntryCd":"OM", "langCd":"en_us"} '  >
						Oman - English</option>
					<option value=' {"cntryCd":"OM", "langCd":"ar_ae"} '  >
						Oman - العربية</option>
					<option value=' {"cntryCd":"PK", "langCd":"en_gb"} '  >
						Pakistan - English</option>
					<option value=' {"cntryCd":"PA", "langCd":"es_latn"} '  >
						Panama - Espanol</option>
					<option value=' {"cntryCd":"PG", "langCd":"en_us"} '  >
						Papua New Guinea - English</option>
					<option value=' {"cntryCd":"PY", "langCd":"es_latn"} '  >
						Paraguay - Espanol</option>
					<option value=' {"cntryCd":"PE", "langCd":"es_latn"} '  >
						Peru - Espanol</option>
					<option value=' {"cntryCd":"PH", "langCd":"en_us"} '  >
						Philippines - English</option>
					<option value=' {"cntryCd":"PL", "langCd":"pl"} '  >
						Poland - Polski</option>
					<option value=' {"cntryCd":"PT", "langCd":"pt_pt"} '  >
						Portugal - Português</option>
					<option value=' {"cntryCd":"PR", "langCd":"es_latn"} '  >
						Puerto Rico - Espanol</option>
					<option value=' {"cntryCd":"QA", "langCd":"ar_ae"} '  >
						Qatar - العربية</option>
					<option value=' {"cntryCd":"RO", "langCd":"ro"} '  >
						Romania - Română</option>
					<option value=' {"cntryCd":"RU", "langCd":"ru"} '  >
						Russian Federation - Русский</option>
					<option value=' {"cntryCd":"RW", "langCd":"en_gb"} '  >
						Rwanda - English</option>
					<option value=' {"cntryCd":"RW", "langCd":"fr_fr"} '  >
						Rwanda - Francais</option>
					<option value=' {"cntryCd":"WS", "langCd":"en_us"} '  >
						Samoa - English</option>
					<option value=' {"cntryCd":"SA", "langCd":"ar_ae"} '  >
						Saudi Arabia - العربية</option>
					<option value=' {"cntryCd":"SN", "langCd":"fr_fr"} '  >
						Senegal - Francais</option>
					<option value=' {"cntryCd":"SN", "langCd":"en_gb"} '  >
						Senegal - English</option>
					<option value=' {"cntryCd":"RS", "langCd":"sr"} '  >
						Serbia - Srpski</option>
					<option value=' {"cntryCd":"SL", "langCd":"fr_fr"} '  >
						Sierra Leone - Francais</option>
					<option value=' {"cntryCd":"SL", "langCd":"en_gb"} '  >
						Sierra Leone - English</option>
					<option value=' {"cntryCd":"SG", "langCd":"en_gb"} '  >
						Singapore - English</option>
					<option value=' {"cntryCd":"SK", "langCd":"sk"} '  >
						Slovakia - Slovenčina</option>
					<option value=' {"cntryCd":"SI", "langCd":"sl"} '  >
						Slovenia - Slovenščina</option>
					<option value=' {"cntryCd":"SB", "langCd":"en_us"} '  >
						Solomon Islands - English</option>
					<option value=' {"cntryCd":"SO", "langCd":"fr_fr"} '  >
						Somalia - Francais</option>
					<option value=' {"cntryCd":"SO", "langCd":"en_gb"} '  >
						Somalia - English</option>
					<option value=' {"cntryCd":"ZA", "langCd":"en_gb"} '  >
						South Africa - English</option>
					<option value=' {"cntryCd":"ES", "langCd":"es_es"} '  >
						Spain - Espanol</option>
					<option value=' {"cntryCd":"LK", "langCd":"en_gb"} '  >
						Sri Lanka - English</option>
					<option value=' {"cntryCd":"KN", "langCd":"en_us"} '  >
						St. Kitts and Nevis - English</option>
					<option value=' {"cntryCd":"KN", "langCd":"es_latn"} '  >
						St. Kitts and Nevis - Espanol</option>
					<option value=' {"cntryCd":"LC", "langCd":"en_us"} '  >
						St. Lucia - English</option>
					<option value=' {"cntryCd":"LC", "langCd":"es_latn"} '  >
						St. Lucia - Espanol</option>
					<option value=' {"cntryCd":"VC", "langCd":"en_us"} '  >
						St. Vincent and the Grenadines - English</option>
					<option value=' {"cntryCd":"VC", "langCd":"es_latn"} '  >
						St. Vincent and the Grenadines - Espanol</option>
					<option value=' {"cntryCd":"SD", "langCd":"en_gb"} '  >
						Sudan - English</option>
					<option value=' {"cntryCd":"SR", "langCd":"en_us"} '  >
						Suriname - English</option>
					<option value=' {"cntryCd":"SR", "langCd":"es_latn"} '  >
						Suriname - Espanol</option>
					<option value=' {"cntryCd":"SZ", "langCd":"fr_fr"} '  >
						Swaziland - Francais</option>
					<option value=' {"cntryCd":"SZ", "langCd":"en_gb"} '  >
						Swaziland - English</option>
					<option value=' {"cntryCd":"SE", "langCd":"sv"} '  >
						Sweden - Svenska</option>
					<option value=' {"cntryCd":"CH", "langCd":"fr_fr"} '  >
						Switzerland - Francais</option>
					<option value=' {"cntryCd":"CH", "langCd":"de"} '  >
						Switzerland - Deutsch</option>
					<option value=' {"cntryCd":"SY", "langCd":"en_gb"} '  >
						Syria - English</option>
					<option value=' {"cntryCd":"TW", "langCd":"zh_tw"} '  >
						Taiwan - 中文&rlm;(繁體)</option>
					<option value=' {"cntryCd":"TJ", "langCd":"en_gb"} '  >
						Tajikistan - English</option>
					<option value=' {"cntryCd":"TZ", "langCd":"fr_fr"} '  >
						Tanzania - Francais</option>
					<option value=' {"cntryCd":"TZ", "langCd":"en_gb"} '  >
						Tanzania - English</option>
					<option value=' {"cntryCd":"TH", "langCd":"th"} '  >
						Thailand - ไทย</option>
					<option value=' {"cntryCd":"TG", "langCd":"en_gb"} '  >
						Togo - English</option>
					<option value=' {"cntryCd":"TG", "langCd":"fr_fr"} '  >
						Togo - Francais</option>
					<option value=' {"cntryCd":"TO", "langCd":"en_us"} '  >
						Tonga - English</option>
					<option value=' {"cntryCd":"TT", "langCd":"en_gb"} '  >
						Trinidad and Tobago - English</option>
					<option value=' {"cntryCd":"TN", "langCd":"fr_fr"} '  >
						Tunisia - Francais</option>
					<option value=' {"cntryCd":"TN", "langCd":"ar_ae"} '  >
						Tunisia - العربية</option>
					<option value=' {"cntryCd":"TR", "langCd":"tr"} '  >
						Turkey - Türkçe</option>
					<option value=' {"cntryCd":"TM", "langCd":"en_gb"} '  >
						Turkmenistan - English</option>
					<option value=' {"cntryCd":"TC", "langCd":"en_us"} '  >
						Turks and Caicos - English</option>
					<option value=' {"cntryCd":"TC", "langCd":"es_latn"} '  >
						Turks and Caicos - Espanol</option>
					<option value=' {"cntryCd":"UG", "langCd":"fr_fr"} '  >
						Uganda - Francais</option>
					<option value=' {"cntryCd":"UG", "langCd":"en_gb"} '  >
						Uganda - English</option>
					<option value=' {"cntryCd":"UA", "langCd":"uk"} '  >
						Ukraine - Українська</option>
					<option value=' {"cntryCd":"AE", "langCd":"en_gb"} '  >
						United Arab Emirates - English</option>
					<option value=' {"cntryCd":"AE", "langCd":"ar_ae"} '  >
						United Arab Emirates - العربية</option>
					<option value=' {"cntryCd":"GB", "langCd":"en_gb"} '  >
						United Kingdom - English</option>
					<option value=' {"cntryCd":"UY", "langCd":"es_latn"} '  >
						Uruguay - Espanol</option>
					<option value=' {"cntryCd":"UZ", "langCd":"en_gb"} '  >
						Uzbekistan - English</option>
					<option value=' {"cntryCd":"UZ", "langCd":"uz"} '  >
						Uzbekistan - O‘zbek tili</option>
					<option value=' {"cntryCd":"VU", "langCd":"en_us"} '  >
						Vanuatu - English</option>
					<option value=' {"cntryCd":"VE", "langCd":"es_latn"} '  >
						Venezuela - Espanol</option>
					<option value=' {"cntryCd":"VN", "langCd":"vi"} '  >
						Vietnam - Tiếng Việt</option>
					<option value=' {"cntryCd":"YE", "langCd":"en_us"} '  >
						Yemen - English</option>
					<option value=' {"cntryCd":"YE", "langCd":"ar_ae"} '  >
						Yemen - العربية</option>
					<option value=' {"cntryCd":"ZM", "langCd":"fr_fr"} '  >
						Zambia - Francais</option>
					<option value=' {"cntryCd":"ZM", "langCd":"en_gb"} '  >
						Zambia - English</option>
					<option value=' {"cntryCd":"ZW", "langCd":"fr_fr"} '  >
						Zimbabwe - Francais</option>
					<option value=' {"cntryCd":"ZW", "langCd":"en_gb"} '  >
						Zimbabwe - English</option>
					</select>


		</div>


		<div class="util">
			<a href="/save/https://account.samsung.com/membership/terms?paramLocale=en_US" target="_blank" >Term & Conditions</a> |
			<a href="/save/https://account.samsung.com/membership/pp?paramLocale=en_US" target="_blank" >Privacy Policy</a>
		</div>
		<div class="copyright">
			Copyright &copy;2013-2019 Samsung Electronics Co., Ltd. All rights reserved.
		</div>

	</div>
</footer>




</body>
</html>





<!--
     FILE ARCHIVED ON 18:45:01 Dec 29, 2019 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 18:45:03 Dec 29, 2019.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
-->
